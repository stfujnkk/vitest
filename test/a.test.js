import { mount } from '@vue/test-utils'
import TodoApp from '@/components/TodoApp.vue'

test('普通测试', () => {
    expect(1).toBe(1)
})


// The component to test
const MessageComponent = {
    template: '<p>{{ msg }}</p>',
    props: ['msg']
}

test('vue 测试', () => {
    const wrapper = mount(MessageComponent, {
        props: {
            msg: 'Hello world'
        }
    })
    // Assert the rendered text of the component
    expect(wrapper.text()).toContain('Hello world')
})
test('vue 导入测试', () => {
    const wrapper = mount(TodoApp)
    const todo = wrapper.get('[data-test="todo"]')
    expect(todo.text()).toBe('Learn Vue.js 3')
})

