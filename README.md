# Vitest

vue3 + vite + jest 模板项目

## Continuous learning

- [vue3](https://v3.cn.vuejs.org/)
- [vue-test-utils](https://test-utils.vuejs.org/guide/)
- [jest](https://www.jestjs.cn/)
- [babel](https://www.babeljs.cn/)
- [lodash](https://www.lodashjs.com/)
- [typescript](https://typescript.bootcss.com/)
- [vite](https://vitejs.cn/)
- [pnpm](https://www.pnpm.cn/)
- [tailwindcss](https://www.tailwindcss.cn/)
- [nuxtjs](https://www.nuxtjs.cn/guide)
- [ssr-fc](http://doc.ssr-fc.com/docs/why)
- [nest.js](https://nestjs.bootcss.com/)